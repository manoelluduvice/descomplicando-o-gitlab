# Descomplicando o Gitlab

Treinamento Desconplicando o Gitlab criado ao vivo na Twich



Day-1
-Entendemos o que é o Git
-Entendemos o que é o Gitlab
-Como criar um grupo no Gitlab
-Como criar um repositório Gitlab
-Aprendemos os comandos básicos para manipulação de arquivos e diretórios no git
-Como criar uma branch
-Como criar um merge request
-Como adicionar um membro no git
-Como fazer o merge na Master/Main
-Como importar um repo do GitHub para o Gitlab
-Mudamos a brach padrão para main
